# Chrome Plugin - CrunchyRoll

This is a simple plugin for filtering videos on Crunchyroll.com
At the moment only filtering videos from the queue when the page is open and the plugin enabled.
The threshold (watch state procentage) is set in the plugin by a default value of 70%.

# Screenshots

## Filtering queue
| unfiltered | filtered |
| ------ | ------ |
| ![](chrome-extension-screenshot.png) | ![](chrome-extension-screenshot-activated.png) |


# Installing the chrome Extension

1. download the extension using this link (version 1.0) https://gitlab.com/Earithramir/chrome-plugin-crunchyroll/-/archive/v1.0/chrome-plugin-crunchyroll-v1.0.zip
2. extract the extension to the disered installation (this is a permanent directory)
2. goto: chrome://extensions/
3. enable `Developer mode` on the right top
4. click the button `Load unpacked`
5. choose the directory where you installed the extension (if you did not rename the directory, the directory will be named something like: `chrome-plugin-crunchyroll-v1.0`)

Thats it!


# Using the extension

In the extensions, the puzzle icon on the right of your address bar, you see an extension with the crunchyroll icon.
Navigate to your Crunchyroll icon, click the extension and in the popup you can now change the watch threshold and toggle the episodes with the hide (show if u click it once) button.


# Features

- 1.0 Set watched % and toggle the hide button to hide the episodes that match the % watched or over.

New features yet to come..
