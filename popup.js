chrome.runtime.onMessage.addListener(function(request, sender) {
    if (request.action == "updatePluginShowCounters") {
        document.getElementById('numberOfShows').innerText = request.episodes;
        document.getElementById('episodesShown').innerText = request.episodesShown;
    }
});

document.addEventListener('DOMContentLoaded', function () {
    var buttonToggleHideWatchedShows = document.getElementById('toggleHideWatchedShows');

    buttonToggleHideWatchedShows.addEventListener('click', function () {
        chrome.tabs.getSelected(null, function (tab) {
            if (tab.url !== 'https://www.crunchyroll.com/home/queue') {
                alert('This feature only works for: https://www.crunchyroll.com/home/queue');
                exit;
            }
            var shouldHideWatchedShows = buttonToggleHideWatchedShows.innerText === 'hide';
            buttonToggleHideWatchedShows.innerText = shouldHideWatchedShows ? 'show' : 'hide';
            var thresholdWatchedPercentage = document.getElementById('thresholdWatchedPercentage').value;
            chrome.tabs.query({ active: true, currentWindow: true }, tabs => {
                chrome.tabs.executeScript(
                    tabs[0].id,
                    { code: 'var page = document.documentElement;'
                        + 'var episodes = page.getElementsByClassName("episode-progress");'
                        + 'hiddenEpisodes = 0;'
                        + 'thresholdWatchedPercentage = ' + thresholdWatchedPercentage + ';'
                        + 'displayState = "' + (shouldHideWatchedShows ? 'none' : 'block') + '";'
                        + 'Array.prototype.forEach.call(episodes, function (episode) {'
                            + 'if (episode.style.width.replace("%", "") >= thresholdWatchedPercentage) {'
                                + 'hiddenEpisodes += ' +(shouldHideWatchedShows ? '1' : '0')  + ';'
                                + 'episode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.style.display = displayState;'
                            + '}'
                        + '});'
                        + 'chrome.runtime.sendMessage({action: "updatePluginShowCounters", episodesShown: episodes.length-hiddenEpisodes, episodes: episodes.length});'
                    }
                );
            });
        });
    }, false);
}, false);
